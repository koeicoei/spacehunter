package nsysu.cse.sapcehunter;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

@SuppressLint("NewApi")
public class GameView extends SurfaceView implements 
SurfaceHolder.Callback, 
Runnable
{
	//動態物件的數量
	private final int itemCount = 2;
	private final int hItemCount = 1;
	private final int bgWidth = 427;
	private final int bgHeight = 640;
	private final int metoCount = 1;
	
	private int lifeCount = 3;
	
	private SurfaceHolder holder;
	private Rect srcRect;
	private Rect destRect;
	private Rect destRect2;
	private int dy, dy2;
	private Bitmap origBG;
	private Bitmap background;
	private Bitmap player;//飛機的圖片
	private Bitmap origPlayer;
	private Bitmap scoreItem[];
	private Bitmap hScoreItem[];
	private ItemPoint p[];
	private ItemPoint p2[];
	private Bitmap lifePic[];//生命圖
	private Bitmap metoPic[];//隕石圖
	private ItemPoint metoPoint[];//隕石位置object
	
	private boolean isRunning = true;
	private Canvas canvas;
	private int screenWidth;
	private int screenHeight;
	private float playerX;
	private float playerY;
	public static final String TAG = "NSYSUAOS";
	private GestureDetectorCompat mDetector;
	private Handler handler = new Handler();
	private int scoreValue = 0;
	private boolean movingFlag = false;
	private Lock touchLock;
	private int drawMeto = 0;//表示某個輪迴會不會有隕石
	
	public GameView(Context context) {
		//parent method
		super(context);
		//設定螢幕大小參數
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		screenWidth = size.x;
		screenHeight = size.y;
		
		Log.d(TAG, "In init X=" + screenWidth);
		Log.d(TAG, "In init Y=" + screenHeight);
		
		this.dy2 = -screenHeight;	//初始化隱藏圖片的位置
		
		this.setFocusable(true);
		holder = this.getHolder();
		holder.addCallback(this);
		srcRect = new Rect(0, 0, this.screenWidth, this.screenHeight);//設定背景圖片大小
		destRect = new Rect(0, dy, screenWidth, screenHeight);  //設定下方背景圖
		destRect2 = new Rect(0, dy2, screenWidth, 0);  //設定上方隱藏背景圖
		
		this.origBG = BitmapFactory.decodeResource(getResources(), R.drawable.background);
		//background = BitmapFactory.decodeResource(getResources(), R.drawable.background);//讀取背景圖
		background = Bitmap.createScaledBitmap(this.origBG, this.screenWidth, this.screenHeight, true);
		this.origPlayer = BitmapFactory.decodeResource(getResources(), R.drawable.spaceship);
		player = Bitmap.createScaledBitmap(this.origPlayer, 80, 80, true);//讀取飛機圖
		
		scoreItem = new Bitmap[this.itemCount];
		p = new ItemPoint[this.itemCount];
		for(int i = 0;i < this.itemCount;i++) {
			this.p[i] = new ItemPoint();
			this.scoreItem[i] = BitmapFactory.decodeResource(getResources(), R.drawable.star);
		}
		
		hScoreItem = new Bitmap[this.hItemCount];
		p2 = new ItemPoint[this.itemCount];
		for(int i=0;i < this.hItemCount;i++) {
			this.p2[i] = new ItemPoint();
			this.hScoreItem[i] = BitmapFactory.decodeResource(getResources(), R.drawable.star2);
		}
		
		/** load生命的圖init **/
		this.lifePic = new Bitmap[this.lifeCount];
		for(int i = 0; i < this.lifeCount; i++) {
			this.lifePic[i] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.heart), 40, 40, true);
		}
		/****/
		
		/**隕石圖init**/
		this.metoPic = new Bitmap[this.metoCount];
		this.metoPoint = new ItemPoint[this.metoCount];
		for(int i = 0; i<this.metoCount; i++) {
			this.metoPoint[i] = new ItemPoint();
			this.metoPic[i] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.stone1), 70, 70, true);
		}
		
		touchLock = new ReentrantLock();
	}
	
	private void drawView() {
		try {
			if(holder != null) {
				canvas = holder.lockCanvas();
				canvas.drawColor(Color.BLACK);
				dy += 6;
				dy2 += 6;
				
				destRect.set(0, dy, this.screenWidth, this.screenHeight + dy);
				destRect2.set(0, dy2-4, this.screenWidth, this.screenHeight + dy2+2);
				
				canvas.drawBitmap(background, srcRect, destRect, null);
				canvas.drawBitmap(background, srcRect, destRect2, null);
				
				if(dy >= screenHeight)
					dy = -screenHeight;
				if(dy2 >= screenHeight) {
					dy2 = -screenHeight;
					this.drawMeto = (int)(Math.random() * 2) + 0;
					Log.d(this.TAG, String.valueOf(this.drawMeto));
				}
				
				canvas.drawBitmap(this.player, this.playerX, this.playerY, null);
				/**低分物件的移動與碰撞判斷**/
				for(int i = 0; i<this.itemCount;i++) {
					this.p[i].Y += 6;
					
					if(this.p[i].Y >= screenHeight) {
						p[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
						p[i].Y = -(float)(Math.random() * (this.screenHeight - 10) + 1);
					}
					 
					/**collision detection algo**/
					if(this.playerX < p[i].X + scoreItem[i].getWidth() &&
							this.playerX + this.player.getWidth() > p[i].X &&
							this.playerY < p[i].Y + scoreItem[i].getHeight() &&
							this.playerY + this.player.getHeight() > p[i].Y) {
						this.scoreValue += 10;
						p[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
						p[i].Y = -(float)(Math.random() * (this.screenHeight - 10) + 1);
					}
					
					/**collision detection algo**/
					
					canvas.drawBitmap(this.scoreItem[i], this.p[i].X, this.p[i].Y, null);
				}
				
				/**高分物件的移動與碰撞判斷**/
				for(int i = 0; i < this.hItemCount; i++) {
					this.p2[i].Y += 11;
					
					if(this.p2[i].Y >= screenHeight) {
						p2[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
						p2[i].Y = -(float)(Math.random() * (this.screenHeight - 10) + 1);
					}
					
					/**collision detection algo**/
					if(this.playerX < p2[i].X + hScoreItem[i].getWidth() &&
							this.playerX + this.player.getWidth() > p2[i].X &&
							this.playerY < p2[i].Y + hScoreItem[i].getHeight() &&
							this.playerY + this.player.getHeight() > p2[i].Y) {
						this.scoreValue += 20;
						p2[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
						p2[i].Y = -(float)(Math.random() * (this.screenHeight - 10) + 1);
					}
					/**collision detection algo**/
					canvas.drawBitmap(this.hScoreItem[i], this.p2[i].X, this.p2[i].Y, null);
				}
				
				/**Score process TODO::FONT-TYPE and FONT-COLOR**/
				Paint score = new Paint();
				score.setColor(Color.RED);
				score.setTextSize(65);
				String formatStr = "%05d";
				String scoreText = String.format(formatStr, this.scoreValue);
				canvas.drawText(scoreText, this.screenWidth - 185, 50, score);
				
				/**render 生命圖**/
				int lorigY = 20;
				for(int i = 0;i < this.lifeCount; i++) {
					canvas.drawBitmap(this.lifePic[i], 20, lorigY, null);
					lorigY += 50;
				}
				/****/
				
				/**隕石code**/
				if(this.drawMeto == 1) {
					for(int i = 0;i < this.metoCount;i++) {
						this.metoPoint[i].Y += 10;
						
						if(this.metoPoint[i].Y >= screenHeight) {
							this.metoPoint[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
							this.metoPoint[i].Y = -(float)(Math.random() * (this.screenHeight - 10) + 1);
						}
						
						if(this.playerX < metoPoint[i].X + metoPic[i].getWidth() &&
								this.playerX + this.player.getWidth() > metoPoint[i].X &&
								this.playerY < metoPoint[i].Y + metoPic[i].getHeight() &&
								this.playerY + this.player.getHeight() > metoPoint[i].Y) {
							//重設定隕石位置
							this.metoPoint[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
							this.metoPoint[i].Y = -(float)(Math.random() * (this.screenHeight - 10) + 1);
							//先停止隕石出現參數，避免位置出現異常
							this.drawMeto = 0;
							//扣掉生命一個
							if(this.lifeCount > 0) {
								this.lifeCount--;
							}
						}
						
						canvas.drawBitmap(this.metoPic[i], this.metoPoint[i].X, this.metoPoint[i].Y, null);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (canvas != null)
				holder.unlockCanvasAndPost(canvas);
		}
	}

	@Override
	public void run() {
		while(isRunning) {
			drawView();
			try {
				Thread.sleep(4);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		new Thread(this).start();
		isRunning = true;
		
		this.playerX = (this.screenWidth - player.getWidth()) / 2;
		this.playerY = this.screenHeight - player.getHeight() - 50;
		
		for(int i = 0;i < this.itemCount;i++) {
			p[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
			p[i].Y = -(float)(Math.random() * (screenHeight - 10) + 1);
		}
		
		/**高分圖形initial位置**/
		for(int i = 0; i < this.hItemCount; i++) {
			p2[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
			p2[i].Y = -(float)(Math.random() * (screenHeight - 10) + 1);
		}
		
		/**隕石圖initial位置**/
		for(int i = 0;i < this.metoCount;i++) {
			this.metoPoint[i].X = (float)(Math.random() * (screenWidth - scoreItem[i].getWidth()) + 1);
			this.metoPoint[i].Y = -(float)(Math.random() * (screenHeight - 10) + 1);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		isRunning = false;
		this.movingFlag = false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK)
			isRunning = false;
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		class MovePlane implements Runnable {
			
			private float X;
			
			public MovePlane(float X) {
				this.X = X;
			}
			
			@Override
			public void run() {
				if(movingFlag) {
					if(this.X < screenWidth / 2) {
						//判斷有沒有碰到螢幕邊界
						if(playerX > 2) {
							playerX -= 10;
						}
					} else {
						if(playerX < (screenWidth - player.getWidth())) {
							playerX += 10;
						}
					}
					handler.postDelayed(this, 20);
				}
			}
		}
		
		/*lock still need to fix*/
		switch(event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touchLock.lock();
			this.movingFlag = true;
			handler.post(new MovePlane(event.getX()));
			touchLock.unlock();
			break;
		case MotionEvent.ACTION_UP:
			touchLock.lock();
			this.movingFlag = false;
			touchLock.unlock();
			break;
		}
			
		return true;
	}
}