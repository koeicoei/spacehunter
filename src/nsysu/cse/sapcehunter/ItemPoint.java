package nsysu.cse.sapcehunter;

public class ItemPoint {
	public float X;
	public float Y;
	
	public ItemPoint() {
		
	}
	
	public ItemPoint(float x, float y) {
		this.X = x;
		this.Y = y;
	}
}
